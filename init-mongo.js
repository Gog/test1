
db.createUser(
    {
      user: "test",
      pwd: "secret",
      roles: [
          {
              role: "readWrite",
              db: "testdb"
          }
      ]
    }
);

let res = [
    db.auth('test', 'secret'),
    db.createCollection('users'),
    db.getCollection('users')
];

printjson(res)
