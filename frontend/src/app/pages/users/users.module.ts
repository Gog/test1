import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import {RouterModule, Routes} from '@angular/router';
import { UsersCreateComponent } from './users-create/users-create.component';
import { UsersGridComponent } from './users-grid/users-grid.component';
import {SharedModule} from '../../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      {
        path: '',
        component: UsersCreateComponent
      },
      {
        path: 'list',
        component: UsersGridComponent
      },
      {
        path: '**',
        redirectTo: ''
      }
    ]
  }
];

@NgModule({
  declarations: [
    UsersComponent,
    UsersCreateComponent,
    UsersGridComponent
  ],
  imports: [
    CommonModule, RouterModule.forChild(routes), SharedModule
  ],
  exports: [RouterModule]
})
export class UsersModule { }
