import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

interface TabData {
  text: string;
  value: string;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  tabs: TabData[];

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    this.tabs =  [
      {
        text: 'Create user',
        value: 'create'
      },
      {
        text: 'List users',
        value: 'list'
      }
    ];
  }

  selectTab(e) {
    const value = this.tabs[e.itemIndex].value;
    this.router.navigate([`/users/${value}`]);
  }

}
