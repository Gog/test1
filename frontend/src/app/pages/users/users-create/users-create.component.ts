import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from "../../../services/api.service";
import notify from "devextreme/ui/notify";
import {DxFormComponent} from "devextreme-angular";

interface GenderData {
  text: string;
  value: string;
  disabled?: boolean;
}

export interface UserFormData {
  firstName: string;
  lastName: string;
  gender: string;
  dateOfBirth: string;
}

@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.scss']
})
export class UsersCreateComponent implements OnInit, AfterViewInit {
  @ViewChild(DxFormComponent, { static: false }) form: DxFormComponent;

  isFormValid: boolean;
  genders: GenderData[];
  formData: UserFormData;
  maxDate: Date = new Date();

  constructor( private api: ApiService ) {
    this.maxDate = new Date(this.maxDate.setFullYear(this.maxDate.getFullYear() - 18));

    this.genders = [{
        text: 'Select gender',
        value: 'none',
        disabled: true
      }, {
        text: 'Man',
        value: 'man'
      }, {
        text: 'Woman',
        value: 'woman'
      }, {
        text: 'Sponge Bob',
        value: 'spongeBob'
      }, {
        text: 'I have not decided yet',
        value: 'unknown'
      }
    ];

    this.formData = {
      firstName: '',
      lastName: '',
      gender: '',
      dateOfBirth: '',
    };
    this.isFormValid = false;
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.validate();
  }

  validate(e?: any) {
    const form = this.form.instance;
    const valid: any = form.validate();
    this.isFormValid = valid.isValid;
  }

  async onFormSubmit(e: any) {
    e.preventDefault();

    try {
      await this.api.createUser(this.formData).toPromise();

      notify({
        message: 'You have submitted the form',
        position: {
          my: 'center top',
          at: 'center top'
        }
      }, 'success', 3000);
    } catch(e) {
      notify({
        message: JSON.stringify(e),
        position: {
          my: 'center top',
          at: 'center top'
        }
      }, 'error', 3000);
    }
  }

}
