import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../../services/api.service";
import {UserFormData} from "../users-create/users-create.component";

@Component({
  selector: 'app-users-grid',
  templateUrl: './users-grid.component.html',
  styleUrls: ['./users-grid.component.scss']
})
export class UsersGridComponent implements OnInit {

  dataSource: any;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  async getUsers() {
    this.dataSource = await this.api.getUsers().toPromise();
  }

  contentReady(e: any) {

  }

  calculateFullName(row: UserFormData) {
    return `${row.firstName} ${row.lastName}`;
  }

}
