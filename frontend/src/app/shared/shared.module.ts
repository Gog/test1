import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DxButtonModule,
  DxCalendarModule,
  DxDataGridModule,
  DxFormModule,
  DxSelectBoxModule,
  DxTabsModule, DxTextBoxModule
} from 'devextreme-angular';

const modules = [
  DxButtonModule,
  DxTabsModule,
  DxCalendarModule,
  DxFormModule,
  DxDataGridModule,
  DxSelectBoxModule,
  DxTextBoxModule
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...modules,
  ],
  exports: [
    ...modules
  ]
})
export class SharedModule { }
