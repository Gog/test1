import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserFormData} from '../pages/users/users-create/users-create.component';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get(`${environment.apiUrl}/list`);
  }

  createUser(data: UserFormData) {
    return this.http.post(`${environment.apiUrl}/add`, data);
  }
}
