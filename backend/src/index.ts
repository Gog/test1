import * as Koa from "koa";
import * as Router from "koa-router";
import * as json from "koa-json";
import * as logger from "koa-logger";
import * as config from "config";
import {connect, connection as db} from 'mongoose'
import * as AutoIncrement from 'mongoose-auto-increment';
import * as cors from '@koa/cors';
import * as bodyParser from 'koa-bodyparser';
import * as http from 'http';

AutoIncrement.initialize(db);

function connectDatabase (uri) {
    db.on('error', err => { console.error('%s', err) })
        .on('close', () => console.log('Database connection closed.'));
    return connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
};

;(async () => {
    try {
        const dbHost = process.env.DB_HOST || config.get<string>('dbHost');
        const dbUser = process.env.DB_USER || config.get<string>('dbUser');
        const dbPassword = process.env.DB_PASSWORD || config.get<string>('dbPassword');
        const dbName = process.env.DB_NAME || config.get<string>('dbName');
        const dbUrl = `mongodb://${dbUser}:${dbPassword}@${dbHost}:27017/${dbName}`;
        console.log(process.env.MONGO_INITDB_DATABASE, dbUrl);
        await connectDatabase(dbUrl);

        console.info(`Connected to ${dbUrl}`);
    } catch (error) {
        console.error(error.toString())
    }

    try {
        const app = new Koa();

        app.use(json());
        app.use(logger());
        app.use(cors());
        app.use(bodyParser());

        const UserRoutes = require("./users/users-routes").default;
        app.use(UserRoutes.routes()).use(UserRoutes.allowedMethods());

        const port = process.env.SERVER_PORT || config.get<string>('port');
        const ipAddress = process.env.SERVER_IP || config.get<string>('ipAddr');
        console.log(111, process.env.SERVER_PORT);
        // @ts-ignore
        const server = http.createServer(app.callback()).listen(port, ipAddress, () => {
            console.log('✅  The server is running at http://' + ipAddress + ':' + port + '/')
        });

    } catch (err) {
        console.error(err);
    }
})();
