import * as Mongoose from 'mongoose';
import * as AutoIncrement from 'mongoose-auto-increment';

const userSchema = new Mongoose.Schema(
    {
        lastName: {
            type: String,
            required: true,
        },
        firstName: {
            type: String,
            required: true,
        },
        gender: {
            type: String,
            required: true,
        },
        dateOfBirth: {
            type: String,
            required: true,
        },
    },
    {timestamps: true}
);

userSchema.method('toClient', function() {
    var obj = this.toObject();

    //Rename fields
    obj.id = obj._id;

    // Delete fields
    delete obj._id;
    delete obj.__v;
    delete obj.createdAt;
    delete obj.updatedAt;

    return obj;
});

userSchema.plugin(AutoIncrement.plugin, {
    model: 'User',
    startAt: 1,
});

Mongoose.model('User', userSchema);

export const UserModel = Mongoose.model('User');
