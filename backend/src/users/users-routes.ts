import * as Router from "koa-router";
import {UsersController} from "./users-controller";

const router = new Router();

router.get("/", async (ctx, next) => {
    ctx.body = { msg: "Hello World!" };
    await next();
});

router.post("/add", async (ctx, next) => {
    ctx.body = await UsersController.create(ctx);
    await next();
});

router.get("/list", async (ctx, next) => {
    ctx.body = await UsersController.list(ctx);
    await next();
});

export default router;
