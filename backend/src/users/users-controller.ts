import {UserModel} from "./users-model";

export const UsersController = {

    list: async (ctx) => {
        const users = await UserModel.find({}).exec();
        for(let i = 0; i < users.length; i++) {
            // @ts-ignore
            users[i] = users[i].toClient();
        }
        return users || [];
    },

    create: async (ctx) => {
        const body = ctx.request.body;
        const user = new UserModel({
            firstName: body.firstName,
            lastName: body.lastName,
            gender: body.gender,
            dateOfBirth: body.dateOfBirth
        });
        await user.save();
        ctx.status = 200;
    }
};
